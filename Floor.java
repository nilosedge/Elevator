public class Floor {
   private boolean occupied;
   private Person person;
   private int floorNumber;
   private Elevator elevator;

   public Floor( int num, Elevator elv) {
      floorNumber = num;
      elevator = elv;
   }

   public void personWalksOntoFloor( Person per ) {
      person = per;
      setFloorOccupied();

        System.out.println("A person has arrived on floor "+floorNumber );
	System.out.println("Person Pushs Call Button");
      if( elevator.getLocation() != floorNumber ) elevator.callElevator( floorNumber );
        System.out.println("Floor "+floorNumber+" Light Comes On.");
      setFloorUnOccupied();
      elevator.personGetsOn( person );
   }

public void personGetsOff( Person per ) {
	System.out.println("Floor "+floorNumber+" Light Turns on." );
	System.out.println("Elevator Doors Open." );
	System.out.println("The person gets off the elevator on floor "+floorNumber);
	System.out.println("Elevator Doors Close." );
	System.out.println("Floor "+floorNumber+" Light Turns off." );
	System.out.println("The person leaves the simulation.\n");
   }

   public void setFloorOccupied() {
      occupied = true;
   }

   public void setFloorUnOccupied() {
      occupied = false;
   }

   public boolean isOccupied() { return occupied; }

}
