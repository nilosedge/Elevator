public class Elevator 
	{
	   private Floor floor1, floor2;
	   private int location;
	
	   public Elevator() 
	   {
	      location = 1;
	   }
	
	   public void registerFloors( Floor f1, Floor f2 ) 
	   {
	      floor1 = f1;
	      floor2 = f2;
	   }
	
	   public void callElevator( int floor ) 
	   {
	      System.out.println("The elevator moves from "+location+" to "+floor);
	      location = floor;
	   }
	
	   public void personGetsOn( Person per ) 
	   {

		System.out.println("Doors on Elevator Open" );
	      System.out.println("The person gets into the elevator." );
		System.out.println("Doors on Elevator Close" );
		System.out.println("Floor "+location+" light goes out" );
	      location = per.getDestinationFloor();
	      System.out.println("Elevator moves from floor "+per.getArrivalFloor()+" to floor " + location);
	      switch( location ) 
	      {
	         case 1 : floor1.personGetsOff( per ); break;
	         case 2 : floor2.personGetsOff( per ); break;
	      }
	   }
	
	   public int getLocation() { return location; }
	
	}
