public class Person {
   private int arrivalFloor;
   private int destinationFloor;

   public void setArrivalFloor( int floor ) { arrivalFloor = floor; }
   public int getArrivalFloor() { return arrivalFloor; }
   public void setDestinationFloor( int floor ) { destinationFloor = floor; }
   public int getDestinationFloor() { return destinationFloor; }

   // Constructor
   public Person( int arrival, int destination ) {
      arrivalFloor = arrival;
      destinationFloor = destination;
  }

}
