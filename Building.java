import java.awt.*;
import java.awt.event.*;
import java.applet.Applet;

public class Building extends Applet implements ActionListener {
   private Button Pfloor1, Pfloor2;
   private Floor floor1, floor2;
   private Elevator elevator;
   private Person person;

   public void init() {
      Pfloor1 = new Button("1");
      Pfloor1.addActionListener( this );
      add( Pfloor1 );
      Pfloor2 = new Button("2");
      Pfloor2.addActionListener( this );
      add( Pfloor2 );
      elevator = new Elevator();
      floor1 = new Floor(1, elevator);
      floor2 = new Floor(2, elevator);
      elevator.registerFloors( floor1, floor2 );
   }

   public void actionPerformed( ActionEvent e ) {
      // Clear both floors of any persons
      floor1.setFloorUnOccupied();
      floor2.setFloorUnOccupied();

      // Determine at random what floor the new person will arrive on
      int arrivalFloor = Integer.parseInt(e.getActionCommand());
      int destinationFloor = (arrivalFloor == 1 ? 2 : 1 );

      person = new Person( arrivalFloor, destinationFloor );

      // Place the person on arrivalFloor
      switch( arrivalFloor) {
         case 1 : floor1.personWalksOntoFloor( person ); break;
         case 2 : floor2.personWalksOntoFloor( person ); break;
         default : showStatus("***** ERROR ***** Invalid arrival floor.");
      }
   }

}
